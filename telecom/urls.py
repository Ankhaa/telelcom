"""telecom URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls import *
from django.contrib.admin import site as admin_site
from filebrowser.sites import site
from django.conf.urls.static import static
from web import views
from web.forms import RecivedMsgForm

urlpatterns = [
    url(r'^$', views.index, name='home'),
    url(r'^products', views.products, name='products'),
    url(r'^services', views.services, name='services'),
    url(r'^industries', views.industries, name='industries'),
    url(r'^aboutus', views.aboutUs, name='aboutus'),
    # url(r'^contact', views.contact, name='contact'),
    url(r'^contact/$', views.RecivedMsgCreateView.as_view(), name='contact'),

    url(r'^c/(?P<category>[-_\w]+)$', 'web.views.product_category', name='product_category'),

    # Examples:
    url(r'^admin/', admin_site.urls),
    url(r'^admin/filebrowser/', site.urls),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^tmceeel/', include('tinymce.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
