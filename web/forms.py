# coding:utf-8
import json

from django import forms

import autocomplete_light
from web.models import RecivedMsg
import bleach
import xml.etree.ElementTree as ET

from tinymce.widgets import TinyMCE
# class RecivedMsgForm(forms.ModelForm):
#     # zurag = CaptchaField()
#     def __init__(self, *args, **kwargs):
#         super(RecivedMsgForm, self).__init__(*args, **kwargs)
#
#     class Meta:
#         model = RecivedMsg
#         fields = ("fname", "lname", "email", "phone", "cname", "ctype", "country", "enquiry", "message")
#         # widgets = {
#         #     'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Зочин', 'width': '200px', }),
#         #     'comment': forms.Textarea(attrs={"class": "form-control", "rows": 3, 'placeholder': 'Сэтгэгдэл бичих'}),
#         # }
#
#     def save(self, request, commit=True):
#         instance = super(RecivedMsgForm, self).save(commit=False)
#         print("ymarchbsan end bn -----------------")
#         if not instance.pk:
#             instance.created_user = request.user
#         instance.updated_user = request.user
#         instance.save()
#         return instance


# class RecivedMsgForm(forms.ModelForm):
#     # zurag = forms.ImageField(label='Зураг', required=False, error_messages={'invalid': "Зөвхөн зураг оруулна уу"}, widget=forms.FileInput)
#     # video_id = forms.CharField(label='Бичлэг', required=False,)
#
#     def __init__(self, *args, **kwargs):
#         super(RecivedMsgForm, self).__init__(*args, **kwargs)
#         # for key in self.fields:
#         #     help_text = ''
#         #     if self.fields[key].help_text:
#         #         help_text = self.fields[key].help_text
#         #     if key == 'body':
#         #         self.fields[key].widget.attrs = {'class': 'form-control', 'placeholder': help_text, 'rows': 6}
#         #     elif key == 'content':
#         #         self.fields[key].widget.attrs = {'class': 'form-control mytinymce', 'placeholder': help_text}
#         #     else:
#         #         self.fields[key].widget.attrs = {'class': 'form-control', 'placeholder': help_text}
#
#     class Meta:
#         model = RecivedMsg
#         fields = ("fname", "lname", "email", "phone", "cname", "ctype", "country", "enquiry", "message")
#
#
#     def save(self, request, commit=True):
#         instance = super(RecivedMsgForm, self).save(commit=False)
#         if not instance.pk:
#             instance.created_user = request.user
#         instance.updated_user = request.user
#         instance.save()
#         return instance


class RecivedMsgForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(RecivedMsgForm, self).__init__(*args, **kwargs)
        for key in self.fields:
            help_text = ''
            if self.fields[key].help_text:
                help_text = self.fields[key].help_text
                self.fields[key].widget.attrs = {'class': 'form-control', 'placeholder': help_text}

    class Meta:
        model = RecivedMsg
        fields = ("fname", "lname", "email", "phone", "cname", "ctype", "country", "enquiry", "message")
