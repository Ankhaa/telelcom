# coding:utf-8
from django.db import models
from filebrowser.fields import FileBrowseField
from tinymce.models import HTMLField
from django.conf import settings
from mptt.models import MPTTModel, TreeForeignKey
from django.core.urlresolvers import reverse


PAGE_HOME = "home"
PAGE_PRO = "product"
PAGE_SER = "service"
PAGE_IND = "industry"
PAGE_ABOUT = "about"
PAGE_CONTACT = "contact"
PAGE_TYPE = (
    (PAGE_HOME, "Home"),
    (PAGE_PRO, "Products"),
    (PAGE_SER, "Services"),
    (PAGE_IND, "Industries"),
    (PAGE_ABOUT, "About Us"),
    (PAGE_CONTACT, "Contact"),
)


# Create your models here.
class Slider(models.Model):
    name = models.CharField(u"Link", max_length=100)
    image = FileBrowseField(u"Image", max_length=400, directory="uploads/slide/", format="image", blank=True, null=True)
    poster = models.TextField(u"poster", help_text=u"poster")
    is_active = models.BooleanField(u"Active", default=False)
    is_show = models.BooleanField(u"Visible", default=True)
    updated_date = models.DateTimeField(u"Update date", auto_now=True)
    read_count   = models.PositiveIntegerField(u"Read count", default=0)

    class Meta:
        verbose_name = u"Slider"
        verbose_name_plural = u"Slider"

    def __unicode__(self):
        return self.name

    @classmethod
    def safe_list(cls):
        return cls.objects.filter(is_show=True)


class Page(models.Model):
    title = models.CharField(u"Title", max_length=300)
    slug = models.CharField(u"Slug", max_length=300, editable=False)
    page_type = models.CharField(u"Page type", max_length=1, choices=PAGE_TYPE, null=True, blank=True)
    zurag = FileBrowseField("Image", max_length=400, directory="uploads/page/", format="image", blank=True, null=True)
    brief = models.TextField(u"Brief", help_text=u"Short content")
    content = HTMLField(u"content", blank=True, null=True)
    is_show = models.BooleanField(u"Visible", default=True)
    created_date = models.DateTimeField(u"Created date", auto_now_add=True)
    updated_date = models.DateTimeField(u"Update date", auto_now=True)
    read_count   = models.PositiveIntegerField(u"Read count", default=0)

    def add_readcount(self):
        self.read_count = F("read_count") + 1
        self.save(read_count_deerees=True)

    @classmethod
    def safe_list(cls):
        return cls.objects.filter(is_show=True)

    @classmethod
    def safe_public_list(cls):
        """Олон нийтэд гарна"""
        return cls.safe_list()

    def page_zurag(self):
        if self.zurag:
            return self.zurag.url

    class Meta:
        verbose_name = u"Page"
        verbose_name_plural = u"Pages"
        ordering = ["-created_date"]

    def __unicode__(self):
        return self.title


class ProductCategory(MPTTModel):
    name = models.CharField(verbose_name=u'Name', unique=True, max_length=100)
    description  = models.CharField(u"Description", max_length=300, null=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', on_delete=models.CASCADE)
    common = models.IntegerField(u"Common search", null=True, blank=True, default=0)
    rank = models.IntegerField(u"Rank", null=True, blank=True, default=0)
    is_show = models.BooleanField(u"Visible", default=True)
    created_date = models.DateTimeField(u"Created date", auto_now_add=True)
    updated_date = models.DateTimeField(u"Updated date", auto_now=True)

    class Meta:
        verbose_name = u"Product category"
        verbose_name_plural = u"Product category"

    class MPTTMeta:
        order_insertion_by = ['name']

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("product_category", kwargs={"category": self.pk})

    @classmethod
    def safe_list(cls):
        return cls.objects.filter(is_show=True)


class Product(MPTTModel):
    name = models.CharField(verbose_name=u'Name', unique=True, max_length=100)
    description  = models.CharField(u"Description", max_length=300, null=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', on_delete=models.CASCADE)
    css_class = models.CharField(u"Style & Icon", max_length=2000, null=True)
    common = models.IntegerField(u"Common search", null=True, blank=True, default=0)
    rank = models.IntegerField(u"Rank", null=True, blank=True, default=0)
    is_show = models.BooleanField(u"Visible", default=True)
    created_date = models.DateTimeField(u"Created date", auto_now_add=True)
    updated_date = models.DateTimeField(u"Updated date", auto_now=True)

    class Meta:
        verbose_name = u"Product"
        verbose_name_plural = u"Products"

    class MPTTMeta:
        order_insertion_by = ['name']

    def __unicode__(self):
        return self.name

    @classmethod
    def safe_list(cls):
        return cls.objects.filter(is_show=True)


class Service(MPTTModel):
    name = models.CharField(verbose_name=u'Name', unique=True, max_length=100)
    description  = models.CharField(u"Description", max_length=300, null=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', on_delete=models.CASCADE)
    css_class = models.CharField(u"Style & Icon", max_length=2000, null=True)
    common = models.IntegerField(u"Common search", null=True, blank=True, default=0)
    rank = models.IntegerField(u"Rank", null=True, blank=True, default=0)
    is_show = models.BooleanField(u"Visible", default=True)
    created_date = models.DateTimeField(u"Created date", auto_now_add=True)
    updated_date = models.DateTimeField(u"Updated date", auto_now=True)

    class Meta:
        verbose_name = u"Service"
        verbose_name_plural = u"Services"

    class MPTTMeta:
        order_insertion_by = ['name']

    def __unicode__(self):
        return self.name

    @classmethod
    def safe_list(cls):
        return cls.objects.filter(is_show=True)


class Industries(MPTTModel):
    name = models.CharField(verbose_name=u'Name', unique=True, max_length=100)
    description  = models.CharField(u"Description", max_length=300, null=True)
    css_class = models.CharField(u"Style & Icon", max_length=2000, null=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', on_delete=models.CASCADE)
    common = models.IntegerField(u"Common search", null=True, blank=True, default=0)
    rank = models.IntegerField(u"Rank", null=True, blank=True, default=0)
    is_show = models.BooleanField(u"Visible", default=True)
    created_date = models.DateTimeField(u"Created date", auto_now_add=True)
    updated_date = models.DateTimeField(u"Updated date", auto_now=True)

    class Meta:
        verbose_name = u"Industry"
        verbose_name_plural = u"Industries"

    class MPTTMeta:
        order_insertion_by = ['name']

    def __unicode__(self):
        return self.name

    @classmethod
    def safe_list(cls):
        return cls.objects.filter(is_show=True)


class Brands(MPTTModel):
    name = models.CharField(verbose_name=u'Name', unique=True, max_length=100)
    image = FileBrowseField(u"Image", max_length=400, directory="uploads/brand/", format="image", blank=True, null=True)
    description  = models.CharField(u"Description", max_length=300, null=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', on_delete=models.CASCADE)
    common = models.IntegerField(u"Common search", null=True, blank=True, default=0)
    rank = models.IntegerField(u"Rank", null=True, blank=True, default=0)
    is_show = models.BooleanField(u"Visible", default=True)
    created_date = models.DateTimeField(u"Created date", auto_now_add=True)
    updated_date = models.DateTimeField(u"Updated date", auto_now=True)

    class Meta:
        verbose_name = u"Brand"
        verbose_name_plural = u"Brands"

    class MPTTMeta:
        order_insertion_by = ['name']

    def __unicode__(self):
        return self.name

    @classmethod
    def safe_list(cls):
        return cls.objects.filter(is_show=True)


class Contact(models.Model):
    address = models.TextField(u"Address", help_text=u"Хаяг")
    post_address = models.CharField(u"Post address", max_length=300, help_text=u"Post address")
    phone1 = models.CharField(u"Phone1", max_length=300, help_text=u"Утас1", blank=True, null=True)
    phone2 = models.CharField(u"Phone2", max_length=300, help_text=u"Утас2", blank=True, null=True)
    phone3 = models.CharField(u"Phone3", max_length=300, help_text=u"Утас3", blank=True, null=True)
    email1 = models.CharField(u"Email1", max_length=300, help_text=u"Email1")
    email2 = models.CharField(u"Email2", max_length=300, help_text=u"Email2", blank=True, null=True)
    fax = models.CharField(u"Fax", max_length=300, help_text=u"Fax")
    website = models.CharField(u"Website", max_length=300, help_text=u"Website")
    facebook = models.CharField(u"Facebook page", max_length=300, help_text=u"Facebook page")

    @classmethod
    def safe_list(cls):
        return cls.objects.filter()

    class Meta:
        verbose_name = u"Contact"
        verbose_name_plural = u"Contact"

    def __unicode__(self):
        return self.address


class AboutUs(models.Model):

    title = models.CharField(u"title", max_length=300)
    image = FileBrowseField("Image", max_length=400, directory="uploads/intro/", format="image", blank=True, null=True)
    brief = models.TextField(u"Brief", help_text=u"Short content")
    content = HTMLField(u"Content", blank=True, null=True)
    # zurag_bg = FileBrowseField("Дэвсгэр Зураг", max_length=400, directory="uploads/introbg/", format="image", blank=True, null=True)

    # facebook = models.CharField(u"Facebook", max_length=300, blank=True, null=True)
    # twitter = models.CharField(u"Twitter", max_length=300, blank=True, null=True)
    # gplus = models.CharField(u"Google plus", max_length=300, blank=True, null=True)
    # instgram = models.CharField(u"Instgram", max_length=300, blank=True, null=True)
    is_show = models.BooleanField(u"Visible", default=True)
    created_date = models.DateTimeField(u"Created date", auto_now_add=True)
    updated_date = models.DateTimeField(u"Updated date", auto_now=True)

    @classmethod
    def safe_list(cls):
        return cls.objects.filter(is_show=True).order_by('created_date').first()

    def intro_zurag(self):
        if self.zurag:
            return self.zurag.url

    class Meta:
        verbose_name = u"About Us"
        verbose_name_plural = u"About Us"
        ordering = ["-created_date"]

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return "%s" % reverse("intro_view", kwargs={"pk": self.pk})


# class Country():
class Country(models.Model):
    country = models.CharField(verbose_name=u'Country name', unique=True, max_length=100)
    flag = FileBrowseField("Flag", max_length=400, directory="uploads/flag/", format="image", blank=True, null=True)
    description  = models.CharField(u"Description", max_length=300, null=True)
    rank = models.IntegerField("Rank", null=True, blank=True, default=0)
    created_date = models.DateTimeField(u"Created date", auto_now_add=True)
    updated_date = models.DateTimeField(u"Updated date", auto_now=True)

    class Meta:
        verbose_name = u"Country"
        verbose_name_plural = u"Country"

    class MPTTMeta:
        order_insertion_by = ['name']

    def __unicode__(self):
        return self.country

    def save(self, *args, **kwargs):
        country = super(Country, self).save(*args, **kwargs)
        return country


class CompanyType(models.Model):
    company_type = models.CharField(verbose_name=u'CompanyType', unique=True, max_length=100)
    description  = models.CharField(u"Description", max_length=300, null=True)
    rank = models.IntegerField("Rank", null=True, blank=True, default=0)
    created_date = models.DateTimeField(u"Created date", auto_now_add=True)
    updated_date = models.DateTimeField(u"Updated date", auto_now=True)

    class Meta:
        verbose_name = u"CompanyType"
        verbose_name_plural = u"CompanyType"

    class MPTTMeta:
        order_insertion_by = ['company_type']

    def __unicode__(self):
        return self.company_type

    def save(self, *args, **kwargs):
        ctype = super(CompanyType, self).save(*args, **kwargs)
        return ctype


class RecivedMsg(models.Model):
    ENQ_CALLBACK = "callback"
    ENQ_QUOTE = "quote"
    ENQ_VISIT = "visit"
    ENQ_EVENT = "event"
    ENQ_OTHER = "other"
    REASON_ENQUIRY = (
        (ENQ_CALLBACK, "Request a call back"),
        (ENQ_QUOTE, "Request a quote"),
        (ENQ_VISIT, "Arrange a visit"),
        (ENQ_EVENT, "Meet an event"),
        (ENQ_OTHER, "Other"),
    )
    fname = models.CharField(verbose_name=u'First Name', unique=True, max_length=100, help_text=u"First name")
    lname = models.CharField(verbose_name=u'Last Name', unique=True, max_length=100, help_text=u"Last name")
    email = models.CharField(u"Email", max_length=300, help_text=u"Email1")
    phone = models.CharField(u"Phone", max_length=300, help_text=u"Phone number", blank=True, null=True)
    cname = models.CharField(verbose_name=u'Company Name', unique=True, max_length=100, help_text=u"Company name")
    ctype = models.ForeignKey(CompanyType, verbose_name=u"Company_type", help_text=u"Company type")
    country = models.ForeignKey(Country, verbose_name=u"Country", help_text=u"Country")
    enquiry = models.CharField("enquiry", choices=REASON_ENQUIRY, default='', max_length=10, help_text=u"Deadline")
    message = models.TextField(u"Message", help_text=u"Message")
    created_date = models.DateTimeField(u"Created date", auto_now_add=True)
    updated_date = models.DateTimeField(u"Updated date", auto_now=True)

    @classmethod
    def safe_list(cls):
        return cls.objects.all().order_by('created_date')

    class Meta:
        verbose_name = u"RecivedMsg"
        verbose_name_plural = u"RecivedMsg"
        ordering = ["-created_date"]

    def __unicode__(self):
        return self.fname

    def save(self, *args, **kwargs):
        msg = super(RecivedMsg, self).save(*args, **kwargs)
        return msg
