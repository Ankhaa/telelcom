from django.contrib import admin
from web.models import Slider, Page, ProductCategory, Product, Service, Industries, Brands, AboutUs, RecivedMsg, CompanyType, Country
# Register your models here.


admin.site.register(Slider)
admin.site.register(Page)
admin.site.register(ProductCategory)
admin.site.register(Product)
admin.site.register(Service)
admin.site.register(Industries)
admin.site.register(Brands)
admin.site.register(AboutUs)
admin.site.register(CompanyType)
admin.site.register(Country)
admin.site.register(RecivedMsg)
