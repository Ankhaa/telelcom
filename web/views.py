#coding:utf-8
from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponse
from web.models import ProductCategory, AboutUs, Service, Brands, RecivedMsg
from web.forms import RecivedMsgForm
from django.views.generic.edit import CreateView
from django.core import mail
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib import messages
from django.core.urlresolvers import reverse


# Create your views here.
def index(request):
    # our_photos = Photos.safe_public_list().filter()[:5]
    brands = Brands.safe_list()
    context = {"activate_menu": 'home', "brands": brands}
    return render(request, 'pages/index.html', context)
    # return HttpResponse("Hello, world. You're at the polls index.")


def products(request):
    # our_photos = Photos.safe_public_list().filter()[:5]
    product_category = ProductCategory.objects.all().order_by('rank')
    context = {'activate_menu': 'product', 'products': products, }
    return render(request, 'pages/products.html', context)
    # return HttpResponse("Hello, world. You're at the polls index.")


def services(request):
    # our_photos = Photos.safe_public_list().filter()[:5]
    service = Service.objects.all().order_by('rank')
    context = {"activate_menu": 'service', 'service': service}
    return render(request, 'pages/services.html', context)
    # return HttpResponse("Hello, world. You're at the polls index.")

def industries(request):
    # our_photos = Photos.safe_public_list().filter()[:5]
    context = {"activate_menu": 'industry',}
    return render(request, 'pages/industries.html', context)
    # return HttpResponse("Hello, world. You're at the polls index.")


def aboutUs(request):
    aboutus = AboutUs.safe_list()
    print aboutus
    context = {
        'activate_menu': 'about',
        'about_us': aboutus,
    }
    return render(request, 'pages/aboutUs.html', context)
    # return HttpResponse("Hello, world. You're at the polls index.")


class RecivedMsgCreateView(CreateView):
    model = RecivedMsg
    form_class = RecivedMsgForm
    template_name = 'pages/contact.html'

    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            # image_ids = form.cleaned_data.get("image_ids")

            x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
            if x_forwarded_for:
                client_ip = x_forwarded_for.split(',')[0]
            else:
                client_ip = request.META.get('REMOTE_ADDR')
            obj = form.save(commit=False)

            obj.client_ip = client_ip
            obj.save()
            form.save_m2m()

            messages.success(request, 'Таны оруулсан мэдээлэл амжилттай бүртгэгдлээ.')
            url = reverse('home', kwargs={'pk': obj.pk})
            return HttpResponseRedirect(url)
        return render(request, self.template_name, {'form': form})

    def form_valid(self, form):
        x_forwarded_for = self.request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            client_ip = x_forwarded_for.split(',')[0]
        else:
            client_ip = self.request.META.get('REMOTE_ADDR')

        obj = form.save(request=self.request, commit=False)
        obj.client_ip = client_ip
        obj.save()
        url = reverse('home')
        return HttpResponseRedirect(url)


def product_category(request, angilal):
    try:
        category = int(category)
    except ValueError:
        raise Http404

    category = ProductCategory.objects.filter(pk=category).first()
    if category is None:
        raise Http404

    # video_list = Video.safe_list().all().exclude(angilal__is_public_list=False)
    # video_list = video_list.filter(angilal=category)

    # video_list = video_list.filter(created_user=request.user)
    return render(
        request,
        'pages/product.html',
        {
            # "video_list": video_list,
            # "TITLE": category,
            'activate_menu': 'product',
        }
    )
