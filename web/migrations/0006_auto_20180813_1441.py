# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0005_auto_20180813_0452'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='brands',
            name='css_class',
        ),
        migrations.RemoveField(
            model_name='brands',
            name=b'status',
        ),
        migrations.RemoveField(
            model_name='industries',
            name=b'status',
        ),
        migrations.RemoveField(
            model_name='product',
            name=b'status',
        ),
        migrations.RemoveField(
            model_name='productcategory',
            name=b'status',
        ),
        migrations.RemoveField(
            model_name='service',
            name=b'status',
        ),
    ]
