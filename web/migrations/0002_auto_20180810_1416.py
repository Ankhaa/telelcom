# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import mptt.fields
import filebrowser.fields
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('address', models.TextField(help_text='\u0425\u0430\u044f\u0433', verbose_name='Address')),
                ('post_address', models.CharField(help_text='Post address', max_length=300, verbose_name='Post address')),
                ('phone1', models.CharField(help_text='\u0423\u0442\u0430\u04411', max_length=300, null=True, verbose_name='Phone1', blank=True)),
                ('phone2', models.CharField(help_text='\u0423\u0442\u0430\u04412', max_length=300, null=True, verbose_name='Phone2', blank=True)),
                ('phone3', models.CharField(help_text='\u0423\u0442\u0430\u04413', max_length=300, null=True, verbose_name='Phone3', blank=True)),
                ('email1', models.CharField(help_text='Email1', max_length=300, verbose_name='Email1')),
                ('email2', models.CharField(help_text='Email2', max_length=300, null=True, verbose_name='Email2', blank=True)),
                ('fax', models.CharField(help_text='Fax', max_length=300, verbose_name='Fax')),
                ('website', models.CharField(help_text='Website', max_length=300, verbose_name='Website')),
                ('facebook', models.CharField(help_text='Facebook page', max_length=300, verbose_name='Facebook page')),
            ],
            options={
                'verbose_name': 'Contact',
                'verbose_name_plural': 'Contact',
            },
        ),
        migrations.AlterModelOptions(
            name='brands',
            options={'verbose_name': 'Brand', 'verbose_name_plural': 'Brands'},
        ),
        migrations.AlterModelOptions(
            name='industries',
            options={'verbose_name': '\u04ae\u0439\u043b\u0434\u0432\u044d\u0440', 'verbose_name_plural': '\u04ae\u0439\u043b\u0434\u0432\u044d\u0440'},
        ),
        migrations.AlterModelOptions(
            name='product',
            options={'verbose_name': 'Product', 'verbose_name_plural': 'Products'},
        ),
        migrations.AlterModelOptions(
            name='productcategory',
            options={'verbose_name': '\u0411\u04af\u0442\u044d\u044d\u0433\u0434\u044d\u0445\u04af\u04af\u043d \u043a\u0430\u0442\u0435\u0433\u043e\u0440', 'verbose_name_plural': '\u0411\u04af\u0442\u044d\u044d\u0433\u0434\u044d\u0445\u04af\u04af\u043d \u043a\u0430\u0442\u0435\u0433\u043e\u0440'},
        ),
        migrations.AlterModelOptions(
            name='service',
            options={'verbose_name': 'Service', 'verbose_name_plural': 'Services'},
        ),
        migrations.RemoveField(
            model_name='product',
            name=b'category',
        ),
        migrations.RemoveField(
            model_name='slider',
            name=b'created_date',
        ),
        migrations.AddField(
            model_name='product',
            name='parent',
            field=mptt.fields.TreeForeignKey(related_name='children', blank=True, to='web.Product', null=True),
        ),
        migrations.AddField(
            model_name='slider',
            name='read_count',
            field=models.PositiveIntegerField(default=0, verbose_name='Read count'),
        ),
        migrations.AlterField(
            model_name='brands',
            name='common',
            field=models.IntegerField(default=0, null=True, verbose_name='Common search', blank=True),
        ),
        migrations.AlterField(
            model_name='brands',
            name='created_date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Created date'),
        ),
        migrations.AlterField(
            model_name='brands',
            name='description',
            field=models.CharField(max_length=300, null=True, verbose_name='\u0422\u0430\u0439\u043b\u0431\u0430\u0440'),
        ),
        migrations.AlterField(
            model_name='brands',
            name='name',
            field=models.CharField(unique=True, max_length=100, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='brands',
            name='rank',
            field=models.IntegerField(default=0, null=True, verbose_name='Rank', blank=True),
        ),
        migrations.AlterField(
            model_name='brands',
            name='status',
            field=models.BooleanField(default=False, verbose_name='Status'),
        ),
        migrations.AlterField(
            model_name='brands',
            name='updated_date',
            field=models.DateTimeField(auto_now=True, verbose_name='Updated date'),
        ),
        migrations.AlterField(
            model_name='industries',
            name='common',
            field=models.IntegerField(default=0, null=True, verbose_name='Common search', blank=True),
        ),
        migrations.AlterField(
            model_name='industries',
            name='created_date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Created date'),
        ),
        migrations.AlterField(
            model_name='industries',
            name='description',
            field=models.CharField(max_length=300, null=True, verbose_name='Description'),
        ),
        migrations.AlterField(
            model_name='industries',
            name='name',
            field=models.CharField(unique=True, max_length=100, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='industries',
            name='rank',
            field=models.IntegerField(default=0, null=True, verbose_name='Rank', blank=True),
        ),
        migrations.AlterField(
            model_name='industries',
            name='status',
            field=models.BooleanField(default=False, verbose_name='Status'),
        ),
        migrations.AlterField(
            model_name='industries',
            name='updated_date',
            field=models.DateTimeField(auto_now=True, verbose_name='Updated date'),
        ),
        migrations.AlterField(
            model_name='page',
            name='brief',
            field=models.TextField(help_text='Short content', verbose_name='Brief'),
        ),
        migrations.AlterField(
            model_name='page',
            name='content',
            field=tinymce.models.HTMLField(null=True, verbose_name='content', blank=True),
        ),
        migrations.AlterField(
            model_name='page',
            name='created_date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Created date'),
        ),
        migrations.AlterField(
            model_name='page',
            name='is_show',
            field=models.BooleanField(default=True, verbose_name='Visible'),
        ),
        migrations.AlterField(
            model_name='page',
            name='read_count',
            field=models.PositiveIntegerField(default=0, verbose_name='Read count'),
        ),
        migrations.AlterField(
            model_name='page',
            name='title',
            field=models.CharField(max_length=300, verbose_name='Title'),
        ),
        migrations.AlterField(
            model_name='page',
            name='updated_date',
            field=models.DateTimeField(auto_now=True, verbose_name='Update date'),
        ),
        migrations.AlterField(
            model_name='page',
            name='zurag',
            field=filebrowser.fields.FileBrowseField(max_length=400, null=True, verbose_name=b'Image', blank=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='common',
            field=models.IntegerField(default=0, null=True, verbose_name='Common search', blank=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='created_date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Created date'),
        ),
        migrations.AlterField(
            model_name='product',
            name='description',
            field=models.CharField(max_length=300, null=True, verbose_name='Description'),
        ),
        migrations.AlterField(
            model_name='product',
            name='name',
            field=models.CharField(unique=True, max_length=100, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='product',
            name='rank',
            field=models.IntegerField(default=0, null=True, verbose_name='Rank', blank=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='status',
            field=models.BooleanField(default=False, verbose_name='Status'),
        ),
        migrations.AlterField(
            model_name='product',
            name='updated_date',
            field=models.DateTimeField(auto_now=True, verbose_name='Updated date'),
        ),
        migrations.AlterField(
            model_name='productcategory',
            name='common',
            field=models.IntegerField(default=0, null=True, verbose_name='Common search', blank=True),
        ),
        migrations.AlterField(
            model_name='productcategory',
            name='created_date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Created date'),
        ),
        migrations.AlterField(
            model_name='productcategory',
            name='description',
            field=models.CharField(max_length=300, null=True, verbose_name='Description'),
        ),
        migrations.AlterField(
            model_name='productcategory',
            name='name',
            field=models.CharField(unique=True, max_length=100, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='productcategory',
            name='rank',
            field=models.IntegerField(default=0, null=True, verbose_name='Rank', blank=True),
        ),
        migrations.AlterField(
            model_name='productcategory',
            name='status',
            field=models.BooleanField(default=False, verbose_name='Status'),
        ),
        migrations.AlterField(
            model_name='productcategory',
            name='updated_date',
            field=models.DateTimeField(auto_now=True, verbose_name='Updated date'),
        ),
        migrations.AlterField(
            model_name='service',
            name='common',
            field=models.IntegerField(default=0, null=True, verbose_name='Common search', blank=True),
        ),
        migrations.AlterField(
            model_name='service',
            name='created_date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Created date'),
        ),
        migrations.AlterField(
            model_name='service',
            name='description',
            field=models.CharField(max_length=300, null=True, verbose_name='Description'),
        ),
        migrations.AlterField(
            model_name='service',
            name='name',
            field=models.CharField(unique=True, max_length=100, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='service',
            name='rank',
            field=models.IntegerField(default=0, null=True, verbose_name='Rank', blank=True),
        ),
        migrations.AlterField(
            model_name='service',
            name='status',
            field=models.BooleanField(default=False, verbose_name='Status'),
        ),
        migrations.AlterField(
            model_name='service',
            name='updated_date',
            field=models.DateTimeField(auto_now=True, verbose_name='Updated date'),
        ),
        migrations.AlterField(
            model_name='slider',
            name='image',
            field=filebrowser.fields.FileBrowseField(max_length=400, null=True, verbose_name='Image', blank=True),
        ),
        migrations.AlterField(
            model_name='slider',
            name='is_active',
            field=models.BooleanField(default=False, verbose_name='Active'),
        ),
        migrations.AlterField(
            model_name='slider',
            name='name',
            field=models.CharField(max_length=100, verbose_name='Link'),
        ),
        migrations.AlterField(
            model_name='slider',
            name='poster',
            field=models.TextField(help_text='poster', verbose_name='poster'),
        ),
        migrations.AlterField(
            model_name='slider',
            name='updated_date',
            field=models.DateTimeField(auto_now=True, verbose_name='Update date'),
        ),
    ]
