# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import filebrowser.fields


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0003_auto_20180811_0153'),
    ]

    operations = [
        migrations.CreateModel(
            name='CompanyType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('company_type', models.CharField(unique=True, max_length=100, verbose_name='CompanyType')),
                ('description', models.CharField(max_length=300, null=True, verbose_name='Description')),
                ('rank', models.IntegerField(default=0, null=True, verbose_name=b'Rank', blank=True)),
                ('created_date', models.DateTimeField(auto_now_add=True, verbose_name='Created date')),
                ('updated_date', models.DateTimeField(auto_now=True, verbose_name='Updated date')),
            ],
            options={
                'verbose_name': 'CompanyType',
                'verbose_name_plural': 'CompanyType',
            },
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('country', models.CharField(unique=True, max_length=100, verbose_name='Country name')),
                ('flag', filebrowser.fields.FileBrowseField(max_length=400, null=True, verbose_name=b'Flag', blank=True)),
                ('description', models.CharField(max_length=300, null=True, verbose_name='Description')),
                ('rank', models.IntegerField(default=0, null=True, verbose_name=b'Rank', blank=True)),
                ('created_date', models.DateTimeField(auto_now_add=True, verbose_name='Created date')),
                ('updated_date', models.DateTimeField(auto_now=True, verbose_name='Updated date')),
            ],
            options={
                'verbose_name': 'Country',
                'verbose_name_plural': 'Country',
            },
        ),
        migrations.CreateModel(
            name='RecivedMsg',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fname', models.CharField(unique=True, max_length=100, verbose_name='First Name')),
                ('lname', models.CharField(unique=True, max_length=100, verbose_name='Last Name')),
                ('email', models.CharField(help_text='Email1', max_length=300, verbose_name='Email')),
                ('phone', models.CharField(help_text='Phone number', max_length=300, null=True, verbose_name='Phone', blank=True)),
                ('cname', models.CharField(unique=True, max_length=100, verbose_name='Company Name')),
                ('enquiry', models.CharField(default=b'', max_length=10, verbose_name=b'enquiry', choices=[(b'callback', b'Request a call back'), (b'quote', b'Request a quote'), (b'visit', b'Arrange a visit'), (b'event', b'Meet an event'), (b'other', b'Other')])),
                ('message', models.TextField(help_text='Message', verbose_name='Message')),
                ('created_date', models.DateTimeField(auto_now_add=True, verbose_name='Created date')),
                ('updated_date', models.DateTimeField(auto_now=True, verbose_name='Updated date')),
                ('country', models.ForeignKey(verbose_name='Country', to='web.Country')),
                ('ctype', models.ForeignKey(verbose_name='Company_type', to='web.CompanyType')),
            ],
            options={
                'ordering': ['-created_date'],
                'verbose_name': 'RecivedMsg',
                'verbose_name_plural': 'About Us',
            },
        ),
        migrations.AddField(
            model_name='brands',
            name='css_class',
            field=models.CharField(max_length=300, null=True, verbose_name='Style & Icon'),
        ),
        migrations.AddField(
            model_name='industries',
            name='css_class',
            field=models.CharField(max_length=300, null=True, verbose_name='Style & Icon'),
        ),
        migrations.AddField(
            model_name='page',
            name='page_type',
            field=models.CharField(blank=True, max_length=1, null=True, verbose_name='Page type', choices=[(b'home', b'Home'), (b'product', b'Products'), (b'service', b'Services'), (b'industry', b'Industries'), (b'about', b'About Us'), (b'contact', b'Contact')]),
        ),
        migrations.AddField(
            model_name='product',
            name='css_class',
            field=models.CharField(max_length=300, null=True, verbose_name='Style & Icon'),
        ),
        migrations.AddField(
            model_name='service',
            name='css_class',
            field=models.CharField(max_length=300, null=True, verbose_name='Style & Icon'),
        ),
    ]
