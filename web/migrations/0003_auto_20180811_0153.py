# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models
import filebrowser.fields


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0002_auto_20180810_1416'),
    ]

    operations = [
        migrations.CreateModel(
            name='AboutUs',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=300, verbose_name='title')),
                ('image', filebrowser.fields.FileBrowseField(max_length=400, null=True, verbose_name=b'Image', blank=True)),
                ('brief', models.TextField(help_text='Short content', verbose_name='Brief')),
                ('content', tinymce.models.HTMLField(null=True, verbose_name='Content', blank=True)),
                ('is_show', models.BooleanField(default=True, verbose_name='Visible')),
                ('created_date', models.DateTimeField(auto_now_add=True, verbose_name='Created date')),
                ('updated_date', models.DateTimeField(auto_now=True, verbose_name='Updated date')),
            ],
            options={
                'ordering': ['-created_date'],
                'verbose_name': 'About Us',
                'verbose_name_plural': 'About Us',
            },
        ),
        migrations.AlterModelOptions(
            name='industries',
            options={'verbose_name': 'Industry', 'verbose_name_plural': 'Industries'},
        ),
        migrations.AlterModelOptions(
            name='productcategory',
            options={'verbose_name': 'Product category', 'verbose_name_plural': 'Product category'},
        ),
        migrations.AddField(
            model_name='brands',
            name='is_show',
            field=models.BooleanField(default=True, verbose_name='Visible'),
        ),
        migrations.AddField(
            model_name='industries',
            name='is_show',
            field=models.BooleanField(default=True, verbose_name='Visible'),
        ),
        migrations.AddField(
            model_name='product',
            name='is_show',
            field=models.BooleanField(default=True, verbose_name='Visible'),
        ),
        migrations.AddField(
            model_name='productcategory',
            name='is_show',
            field=models.BooleanField(default=True, verbose_name='Visible'),
        ),
        migrations.AddField(
            model_name='service',
            name='is_show',
            field=models.BooleanField(default=True, verbose_name='Visible'),
        ),
        migrations.AddField(
            model_name='slider',
            name='is_show',
            field=models.BooleanField(default=True, verbose_name='Visible'),
        ),
        migrations.AlterField(
            model_name='brands',
            name=b'description',
            field=models.CharField(max_length=300, null=True, verbose_name='Description'),
        ),
    ]
