# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import filebrowser.fields


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0006_auto_20180813_1441'),
    ]

    operations = [
        migrations.AddField(
            model_name='brands',
            name='image',
            field=filebrowser.fields.FileBrowseField(max_length=400, null=True, verbose_name='Image', blank=True),
        ),
    ]
