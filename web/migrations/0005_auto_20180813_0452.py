# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0004_auto_20180813_0443'),
    ]

    operations = [
        migrations.AlterField(
            model_name='brands',
            name='css_class',
            field=models.CharField(max_length=2000, null=True, verbose_name='Style & Icon'),
        ),
        migrations.AlterField(
            model_name='industries',
            name='css_class',
            field=models.CharField(max_length=2000, null=True, verbose_name='Style & Icon'),
        ),
        migrations.AlterField(
            model_name='product',
            name='css_class',
            field=models.CharField(max_length=2000, null=True, verbose_name='Style & Icon'),
        ),
        migrations.AlterField(
            model_name='service',
            name='css_class',
            field=models.CharField(max_length=2000, null=True, verbose_name='Style & Icon'),
        ),
    ]
